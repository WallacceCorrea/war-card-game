const deckCards = document.querySelector(".deck-cards-btn");
const drawCards = document.querySelector(".draw-cards-btn");
const winner = document.querySelector(".winner");
const remainCards = document.querySelector("#remaining-cards");
const p1Score = document.querySelector("#player-1");
const p2Score = document.querySelector("#player-2");
const cards = document.querySelector(".cards");

const valuesArr = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "JACK",
  "QUEEN",
  "KING",
  "ACE",
];

let deckId = "";
let cardValue = [];
let scoreComputer = 0;
let scorePlayer = 0;
let isComputer = true;
let isCardOver = false;

drawCards.addEventListener("click", handleCards);
deckCards.addEventListener("click", handleClick);

function handleClick() {
  fetch("https://apis.scrimba.com/deckofcards/api/deck/new/shuffle/")
    .then((res) => res.json())
    .then((data) => {
      deckId = data.deck_id;
      remainCards.textContent = `Remaining cards: ${data.remaining}`;
    });
    resetGame()
}

function handleCards() {
  fetch(`https://apis.scrimba.com/deckofcards/api/deck/${deckId}/draw/?count=2`)
    .then((res) => res.json())
    .then((data) => {
      getCard(data.cards);
      getRemainCards(data);
      if (data.remaining === 0) {
        disableDraw();
        selectWinner(scoreComputer, scorePlayer)
      } else if(data.remaining === 2) {
        isCardOver = true
      }
    });
  isComputer = !isComputer;
}

function getCard(cards) {
  let html = "";
  cardValue = [];
  for (let card of cards) {
    html += `<img class="card" src="${card.image}">`;
    cardValue.push(card.value);
  }
  render(html);
  calcCardsValue(cardValue);
}

function render(data) {
  cards.innerHTML = data;
}

function calcCardsValue(cardsArr) {
  let card1 = "";
  let card2 = "";

  for (let index of valuesArr) {
    if (cardsArr[0] === index) {
      card1 = valuesArr.indexOf(index);
    } else if (cardsArr[1] === index) {
      card2 = valuesArr.indexOf(index);
    }
  }
  selectWinner(card1, card2);
  calcScore(card1, card2);
}

function selectWinner(computer, player) {
  if (computer > player) {
    renderResult("Computer Win");
  } else if (computer === player) {
    renderResult("It's a Tie!");
  } else {
    renderResult("You Win");
  }
}

function calcScore(computer, player) {
  scoreComputer += parseInt(computer + 2);
  scorePlayer += parseInt(player + 2);
  renderScore(scoreComputer, scorePlayer);
}

function renderResult(result) {
  if (!isCardOver) {
    winner.textContent = result;
  } else {
    winner.textContent = result + " The Game!";
  }
}

function getRemainCards(data) {
  remainCards.textContent = `Remaining cards: ${data.remaining}`;
}

function disableDraw() {
  drawCards.disabled = true;
}

function renderScore(scoreComputer, scorePlayer) {
  p1Score.textContent = `Computer: ${scoreComputer}`;
  p2Score.textContent = `Player: ${scorePlayer}`;
}

function resetGame() {
  drawCards.disabled = false;
  isCardOver = false
  scoreComputer = 0
  scorePlayer = 0
  renderScore(0,0)
  winner.textContent = "War Game!"
}

handleClick();
